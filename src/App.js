import React, { useState, useEffect } from 'react';

import ClassComponent from './components/ClassComponent/ClassComponent';
import FunctionalComponent from './components/FunctionalComponent/FunctionalComponent';
import TestComponent from './components/TestComponent/TestComponent';

import logo from './logo.svg';
import './App.css';

function App() {
  const [oneMessage, setOneMessage] = useState('');

  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}

      <ClassComponent
      // exampleProps={{ id: 1, msg: 'First message' }}
      // oneMessage={oneMessage}
      // setOneMessage={setOneMessage}
      />
      <FunctionalComponent
      // exampleProps={{ id: 1, msg: 'First message' }}
      // oneMessage={oneMessage}
      // setOneMessage={setOneMessage}
      />
      {/* <TestComponent oneMessage={oneMessage} setOneMessage={setOneMessage} /> */}
    </div>
  );
}

export default App;
