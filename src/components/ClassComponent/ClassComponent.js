import React, { Component } from 'react';

import './ClassComponent.css';

class ClassComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: 'React is awesome',
    };
  }

  render() {
    return (
      <div className="class-component component">
        <h1>Class Component</h1>
        {this.state.message && (
          <p>
            <b>Message: </b>
            {this.props.message ? this.props.message : this.state.message}
          </p>
        )}
        <button onClick={() => this.setState({ message: 'Have a nice day!' })}>
          Set Message
        </button>

        {/* <p>
          {this.props.exampleProps.id}. {this.props.exampleProps.msg}
        </p> */}

        {/* <p>
          <b>One Message:</b> {this.props.oneMessage}
        </p>
        <button
          onClick={() =>
            this.props.setOneMessage('One message set from Class component')
          }
        >
          Set One Message
        </button> */}
      </div>
    );
  }
}

export default ClassComponent;
