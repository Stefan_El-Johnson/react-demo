import React, { useState, useEffect } from 'react';

import './FunctionalComponent.css';

function FunctionalComponent(props) {
  const [message, setMessage] = useState('a');

  useEffect(() => {
    console.log('Component mounted.', props, message);
  }, []);

  useEffect(() => {
    console.log('props changed!', props);
  }, [props]);

  useEffect(() => {
    console.log('message changed!', message);
  }, [message]);

  return (
    <div className="functional-component component">
      <h1>Functional Component</h1>

      <p>
        <b>Message:</b> {message}
      </p>
      <button onClick={() => setMessage('Have a nice day!')}>
        Set Message
      </button>

      {/* <p>
        {props.exampleProps.id}. {props.exampleProps.msg}
      </p> */}

      {/* <p>
        <b>One Message:</b> {props.oneMessage}
      </p>
      <button
        onClick={() =>
          props.setOneMessage('One Message set from functional component')
        }
      >
        Set One Message
      </button> */}
    </div>
  );
}

export default FunctionalComponent;
