import React, { useState, useEffect } from 'react';

import './TestComponent.css';

function TestComponent(props) {
  return (
    <div className="test-component component">
      <h1>Test Component</h1>

      <p>
        <b>One Message:</b> {props.oneMessage}
      </p>

      <button
        onClick={() => props.setOneMessage('This is the one true message')}
      >
        Set One Message
      </button>
    </div>
  );
}

export default TestComponent;
